# FM301 C++ API and Toolkit

# Prerequisites
The only runtime dependency strictly by the FM301 C++ API (other than standard system libraries) is the NetCDF C
library.  The HDF5 C library is also optionally required to enable conversion to and from the ODIM_H5 format.

This API is written in C++20, which means that a fairly recent C++ compiler will be required to build the software.
For recent versions of most operating systems, the default C++ compiler available from the system will be adequate.
For some older operating systems it may be necessary to install a more recent compiler.  A version of CMake >= 3.10
is also required to manage the build process.

Specific instructions to create an appropriate build environment for several known operating systems are shown
below:

#### CentOS 7
Install the `cmake3` package to obtain a suitable version of CMake, and the the `devtoolset-10` package to obtain
a suitable version of the GCC compiler.  Note that the `scl enable devtoolset-10 bash` line must be run in any
terminal where you will run `cmake` or `make` commands to ensure the correct compiler is selected.  You will also need to
substitute `cmake3` for `cmake` when building the software as instructed below.

```bash
sudo yum install -y epel-release centos-release-scl
sudo yum install -y cmake3 devtoolset-10 netcdf-devel hdf5-devel
scl enable devtoolset-10 bash
```

#### CentOS 8
Install the `gcc-toolset-10` package to obtain a suitable version of GCC.  Note that the `scl enable gcc-toolset-10 bash`
command must be run in any terminal where you will run `cmake` or `make` commands to ensure the correct compiler is
selected.

```bash
sudo dnf install -y dnf-plugins-core epel-release
sudo dnf config-manager --enable powertools
sudo dnf install -y cmake gcc-toolset-10 netcdf-devel hdf5-devel
scl enable gcc-toolset-10 bash
```

#### RHEL 8
Install the `gcc-toolset-10` package to obtain a suitable version of GCC.  Note that the `scl enable gcc-toolset-10 bash`
command must be run in any terminal where you will run `cmake` or `make` commands to ensure the correct compiler is
selected.

```bash
sudo dnf install -y dnf-plugins-core redhat-rpm-config
sudo dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
sudo dnf config-manager --enable codeready-builder-for-rhel-8-rhui-rpms
sudo dnf install -y cmake gcc-toolset-10 netcdf-devel hdf5-devel hdf5-static
scl enable gcc-toolset-10 bash
```

#### Fedora 34
The default C++ compiler in recent versions of Fedora contains adequate C++20 support to build the API.  Ensure that
the compiler and `netcdf`/`hdf5` development packages are installed:

```bash
sudo dnf install gcc-c++ netcdf-devel hdf5-devel
```

# Compilation
To compile and install the library to the default system prefix (typically `/usr/local`) use the following commands:

```bash
mkdir build
cd build
cmake ..
make
sudo make install
```

To compile and install to a custom prefix supply the `-DCMAKE_INSTALL_PREFIX` argument to the `cmake` command:

```bash
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=${HOME}/local
make
make install
```

# Tools
## Format conversion utility
The `fm301-convert` tool allows volume data to be converted between FM301 and other level 2 radar
data formats.  Currently the only supported format is the OPERA Data Information Model for HDF5 (ODIM_H5).

Use of the tool is as simple as supplying an input and output file paths as command line parameters:

```bash
fm301-convert input.h5 output.nc
```

The source and destination file formats will be automatically detected based on the file extensions, but may
also be explicitly set using the `--format-in` and `--format-out` parameters:

```bash
fm301-convert --format-in=fm301 --format-out=odim_h5 input.file output.file
```

Run `fm301-convert --help` for further information.

# API Usage
Detailed documentation of the C++ API is planned but not yet complete.  Please refer to the main
[`fm301.h`](src/fm301.h) header as a reference for the classes and functions that are available.
Some basic code examples are also shown in the sections below:

## Basic file writing
The code below shows an example of writing a minimal FM301 file which represents a volume containing
one sweeps and one dataset (moment).  The `volume` class acts as a handle to the file, and exposes functions
for setting metadata and adding sweeps.  Similarly the `sweep` class is a handle to a single sweep group
within the file and exposes functions for setting sweep, ray, and range bin level metadata as well as adding
datasets.  Finally the `dataset` class is a handle to a single dataset variable within a sweep group.  It
exposes functions to set metadata, and write the dataset contents.

```c++
#include <fm301.h>

#include <chrono>
#include <iostream>
#include <vector>

using namespace fm301;

int main(int argc, char const* argv[])
try
{
  // some static data (comes from your radar application)
  auto nrays = 360;
  auto nbins = 500;
  auto dbz = std::vector<float>(nrays * nbins);
  auto times = std::vector<std::chrono::duration<double>>(nrays);
  auto azimuths = std::vector<float>(nrays);
  auto ranges = std::vector<float>(nbins);
  auto start_time = std::chrono::system_clock::now();

  // write the volume to a new file
  auto vol = volume{"my_file.nc", io_mode::create};
  vol.set_wmo_wsi("0-20010-0-94865");
  vol.set_latitude(-37.852);
  vol.set_longitude(144.752);
  vol.set_altitude(44.0);
  vol.set_time_coverage_start(start_time);

  auto swp = vol.sweep_add(nrays, nbins, 1, 1);
  swp.set_fixed_angle(0.5);
  swp.ray_set_time(times, start_time);
  swp.ray_set_azimuth(azimuths);
  swp.bin_set_range(ranges);

  auto dat = swp.dataset_add(standard_quantity::dbzh, data_type::u8, 5);
  dat.set_fill_value(uint8_t(0));
  dat.set_scale_factor(0.5f);
  dat.set_add_offset(-32.0f);
  dat.write(dbz);

  return 0;
}
catch (std::exception& err)
{
  std::cerr << "Failed to write fm301 file: " << err.what() << std::endl;
  return 1;
}
```

Note that the above example omits setting a large number of mandatory metadata and will therefore not produce a fully
compliant FM301 file.  Refer to the FM301 standard to determine which metadata should be included in all
files.

## Basic file reading
The code below shows a simple example of reading data from a FM301 file.

```c++
#include <fm301.h>

#include <chrono>
#include <iostream>
#include <vector>

using namespace fm301;

int main(int argc, char const* argv[])
try
{
  auto vol = volume{"my_file.nc", io_mode::read_only};

  // access metadata using .value() to throw an exception if it is not found
  auto latitude = vol.latitude().value();
  auto longitude = vol.longitude().value();
  auto altitude = vol.altitude().value();

  // access metadata using a condtional to check presence before use
  if (auto wsi = vol.wmo_wsi())
    std::cout << "WSI: " << *wsi << std::endl;

  // loop through sweeps in the volume
  for (size_t isweep = 0; isweep < vol.sweep_count(); ++isweep)
  {
    auto swp = vol.sweep(isweep);
    auto elevation = swp.fixed_angle().value();

    // read a specific radar quantity
    auto dbzh = std::vector<float>(swp.ray_count() * swp.bin_count());
    if (auto dat = swp.dataset(standard_quantity::dbzh))
      dat->read(dbzh, std::numeric_limits<float>::quiet_NaN(), -999.0f);

    // check for and read a custom dataset
    auto beamb = std::vector<float>(swp.ray_count() * swp.bin_count());
    if (auto dat = swp.dataset("BEAMB"))
      dat->read(beamb);
  }

  return 0;
}
catch (std::exception& err)
{
  std::cerr << "Failed to write fm301 file: " << err.what() << std::endl;
  return 1;
}
```

## Standardized metadata access
Each standardized metadatum `foo` associated with an object can be read and written using functions named
`foo()` and `set_foo()` respectively.  The read runction (`foo()`) for a metadatum always returns a
`std::optional<T>` object, where `T` matches the expected type specified in the FM301 standard.  The
write function (`set_foo()`) typically takes `T` or `const T&` argument.

The use of `std::optional` when reading metadata makes it easy for client code to check whether a metadatum
is provided by the file without requiring a separate function (or function call) just to check for its
existance.  This pattern is followed regardless of whether a metadatum is specified as mandatory by the
FM301 standard.

A typical way to read a metadatum is shown below:

```c++
if (auto val = object.foo())
  use_foo(*val);
```

In this example, an explicit `if` statement around the accessor `foo()` call is used to skip processing of
the metadatum if it is not present.  Once presence of the `foo` metadata is confirmed, the value may be
retrieved from the `std::optional` by dereferencing it as seen in `use_foo(*val)`.

For mandatory metadata, users may find it more convenient to use the `value()` function of `std::optional`
as shown below:

```c++
use_foo(object.foo().value());
```

Since the `value()` function will throw an exception if the `std::optional` returned by `foo()` is empty,
this code is both compact and safe.  If the mandatory metadata `foo` is not found in the file, an exception
will be thrown instead of calling `use_foo()` with an invalid value.

## Metadata for rays and range bins
The `sweep` class contains functions for accessing metadata related to the sweep itself (scalars), the
collection of rays rays (`time` dimension), or the collection of range bins (`range` dimension).
To differentiate between functions that get and set metadata on these different types of objects, the
functions for interating with metadata related to rays and range bins are prefixed with `ray_` and `bin_`
respectively.

For example, the fragment below shows writing the per-ray azimuth angle metadata for all the rays within
a sweep.  Note that the `set_foo()` style function is prefixed with `ray_`:

```c++
sweep.ray_set_azimuth(azimuths);
```

## Custom metadata access
User defined (i.e. non-standardized) metadata is supported on all conceptual objects of the file format
including the volume, sweeps, rays, range bins, and datasets.  Since metadata may be stored as either
NetCDF attributes or variables, the following generic metadata access functions are provided by the
volume and sweep classes:

```c++
    /// Check whether a metadata attribute exists
    auto attribute_exists(char const* name) const -> bool;
    auto attribute_exists(std::string const& name) const -> bool;

    /// Get a metadata attribute if present
    auto attribute_get(char const* name) const -> std::optional<metadata_value>;
    auto attribute_get(std::string const& name) const -> std::optional<metadata_value>;

    /// Set a metadata attribute
    auto attribute_set(char const* name, metadata_value const& val) -> void;
    auto attribute_set(std::string const& name, metadata_value const& val) -> void;

    /// Erase a metadata attribute
    auto attribute_erase(char const* name) -> void;
    auto attribute_erase(std::string const& name) -> void;

    /// Check whether a variable exists
    auto variable_exists(char const* name) const -> bool;
    auto variable_exists(std::string const& name) const -> bool;

    /// Get a scalar metadata variable if present
    auto variable_get(char const* name) const -> std::optional<metadata_scalar>;
    auto variable_get(std::string const& name) const -> std::optional<metadata_scalar>;

    /// Set a scalar metadata variable
    auto variable_set(char const* name, metadata_scalar const& val) -> void;
    auto variable_set(std::string const& name, metadata_scalar const& val) -> void;
```

The `metadata_value` and `metadata_scalar` types here are `std::variant` type-safe unions that allow reading
of metadata into any of the known supported types.  The user can then use the interface of the `std::variant`
class to ensure that the expected type is present.

For custom ray and range bins related metadata, only variable may be used, and the these variables must be
use the `time` and `range` dimensions repsectively.  As with the standardized metadata, similar functions to
the above are provided, but prefixed with `ray_` and `bin_` as appropriate:

```c++
    /// Get generic per-ray metadata
    auto ray_variable_get(char const* name) const -> std::optional<metadata_vector>;
    auto ray_variable_get(std::string const& name) const -> std::optional<metadata_vector>;

    /// Set generic per-ray metadata
    auto ray_variable_set(char const* name, metadata_vector const& val) -> void;
    auto ray_variable_set(std::string const& name, metadata_vector const& val) -> void;

    /// Get generic per-range bin metadata
    auto bin_variable_get(char const* name) const -> std::optional<metadata_vector>;
    auto bin_variable_get(std::string const& name) const -> std::optional<metadata_vector>;

    /// Set generic per-range bin metadata
    auto bin_variable_set(char const* name, metadata_vector const& val) -> void;
    auto bin_variable_set(std::string const& name, metadata_vector const& val) -> void;
```

The `metadata_vector` type is a `std::variant` type-safe union similar to the `metadata_scalar` type with the
difference that each member is a `std::vector`.  The length of the vector will (reading) or must (writing)
match the number of rays or bins in the sweep as appropriate.  An exception will be thrown if this is not
the case.
