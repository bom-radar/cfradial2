/*---------------------------------------------------------------------------------------------------------------------
 * FM301 C++ API and Toolkit
 *
 * Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *-------------------------------------------------------------------------------------------------------------------*/
#include "fm301.h"
#include "ncutils.h"
#include <netcdf.h>
#include <algorithm>
#include <cstring>

using namespace fm301;

quantity_traits standard_quantity_metadata[] =
{
    { "DBZH", "radar_equivalent_reflectivity_factor_h", "Equivalent reflectivity factor H", "dBZ" }
  , { "DBZV", "radar_equivalent_reflectivity_factor_v", "Equivalent reflectivity factor V", "dBZ" }
  , { "ZH", "radar_linear_equivalent_reflectivity_factor_h", "Linear equivalent reflectivity factor H", "mm6 m-3" }
  , { "ZV", "radar_linear_equivalent_reflectivity_factor_v", "Linear equivalent reflectivity factor V", "mm6 m-3" }
  , { "DBTH", "radar_equivalent_reflectivity_factor_h", "Total power H (uncorrected reflectivity)", "dBZ" }
  , { "DBTV", "radar_equivalent_reflectivity_factor_v", "Total power V (uncorrected reflectivity)", "dBZ" }
  , { "TH", "radar_linear_equivalent_reflectivity_factor_h", "Linear total power H (uncorrected reflectivity)", "mm6 m-3" }
  , { "TV", "radar_linear_equivalent_reflectivity_factor_v", "Linear total power V (uncorrected reflectivity)", "mm6 m-3" }
  , { "VRADH", "radial_velocity_of_scatterers_away_from_instrument_h", "Radial velocity of scatterers away from instrument H", "m s-1" }
  , { "VRADV", "radial_velocity_of_scatterers_away_from_instrument_v", "Radial velocity of scatterers away from instrument V", "m s-1" }
  , { "WRADH", "radar_doppler_spectrum_width_h", "Doppler spectrum width H", "m s-1" }
  , { "WRADV", "radar_doppler_spectrum_width_v", "Doppler spectrum width V", "m s-1" }
  , { "ZDR", "radar_differential_reflectivity_hv", "Log differential reflectivity H/V", "dB" }
  , { "LDR", "radar_linear_depolarization_ratio", "Log-linear depolarization ratio HV", "dB" }
  , { "LDRH", "radar_linear_depolarization_ratio_h", "Log-linear depolarization ratio H", "dB" }
  , { "LDRV", "radar_linear_depolarization_ratio_v", "Log-linear depolarization ratio V", "dB" }
  , { "PHIDP", "radar_differential_phase_hv", "Differential phase HV", "degree" }
  , { "KDP", "radar_specific_differential_phase_hv", "Specific differential phase HV", "degree km-1" }
  , { "PHIHX", "radar_differential_phase_copolar_h_crosspolar_v", "Cross-polar differential phase", "degree" }
  , { "RHOHV", "radar_correlation_coefficient_hv", "Correlation coefficient HV", "1" }
  , { "RHOHX", "radar_correlation_coefficient_copolar_h_crosspolar_v", "Co-to-cross polar correlation coefficient H", "1" }
  , { "RHOXV", "radar_correlation_coefficient_copolar_v_crosspolar_h", "Co-to-cross polar correlation coefficient V", "1" }
  , { "DBM", "radar_received_signal_power", "Log power", "dBm" }
  , { "DBMHC", "radar_received_signal_power_copolar_h", "Log power co-polar H", "dBm" }
  , { "DBMHX", "radar_received_signal_power_crosspolar_h", "Log power cross-polar H", "dBm" }
  , { "DBMVC", "radar_received_signal_power_copolar_v", "Log power co-polar V", "dBm" }
  , { "DBMVX", "radar_received_signal_power_crosspolar_v", "Log power cross-polar V", "dBm" }
  , { "SNR", "radar_signal_to_noise_ratio", "Signal-to-noise ratio", "dB" }
  , { "SNRHC", "radar_signal_to_noise_ratio_copolar_h", "Signal-to-noise ratio co-polar H", "dB" }
  , { "SNRHX", "radar_signal_to_noise_ratio_crosspolar_h", "Signal-to-noise ratio cross-polar H", "dB" }
  , { "SNRVC", "radar_signal_to_noise_ratio_copolar_v", "Signal-to-noise ratio co-polar V", "dB" }
  , { "SNRVX", "radar_signal_to_noise_ratio_crosspolar_v", "Signal to noise ratio cross polar V", "dB" }
  , { "NCP", "radar_normalized_coherent_power", "Normalized coherent power", "1" }
  , { "NCPH", "radar_normalized_coherent_power_h", "Normalized coherent power co-polar H", "1" }
  , { "NCPV", "radar_normalized_coherent_power_v", "Normalized coherent power co-polar V", "1" }
  , { "RR", "radar_estimated_precipitation_rate", "Rain rate", "mm/hr" }
  , { "REC", "radar_scatterer_classification", "Radar echo classification", "" }
};

auto fm301::standard_quantity_traits(standard_quantity quantity) -> quantity_traits const&
{
  return standard_quantity_metadata[static_cast<int>(quantity)];
}

static inline auto format_bool(bool val) -> char const*
{
  return val ? "true" : "false";
}

static inline auto parse_bool(std::optional<std::string> const& val) -> std::optional<bool>
{
  if (!val)
    return std::nullopt;
  if (val == "true")
    return true;
  if (val == "false")
    return false;
  throw std::runtime_error{"Invalid Boolean attribute"};
}

static auto format_iso8601_time(std::chrono::system_clock::time_point time) -> std::string
{
  auto tt = std::chrono::system_clock::to_time_t(time);

  struct tm tmm;
  if (gmtime_r(&tt, &tmm) == nullptr)
    throw std::runtime_error{"Failure in gmtime_r"};

  char buf[64];
  auto len = strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%SZ", &tmm);
  if (len <= 0)
    throw std::runtime_error{"Failure in strftime"};

  return std::string(buf);
}

static auto parse_iso8601_time(std::string const& val) -> std::chrono::system_clock::time_point
{
  char t;
  char tzone[8];
  struct tm tmm;
  memset(&tmm, 0, sizeof(struct tm));
  tmm.tm_mon = 1;
  tmm.tm_mday = 1;

  auto ret = sscanf(
        val.c_str()
      , "%4d-%2d-%2d%c%2d:%2d:%2d%7s"
      , &tmm.tm_year
      , &tmm.tm_mon
      , &tmm.tm_mday
      , &t
      , &tmm.tm_hour
      , &tmm.tm_min
      , &tmm.tm_sec
      , tzone);
  if (ret < 3 || ret == 4 || ret == 5 || (ret >= 6 && t != ' ' && t != 'T'))
    throw std::runtime_error{"Failed to parse iso8601 time"};

  // adjust tm structure offsets
  tmm.tm_year -= 1900;
  tmm.tm_mon -= 1;

  // determine the time zone adjustment
  /* CF allows a nightmarish number of different combinations for the timezone: signed or unsigned,
   * with a separating colon, and 1, 2, 3 or 4 digit representations without a colon. */
  auto tz_delta = 0;
  if (ret == 8 && strncmp(tzone, "Z", 7) != 0 && strncmp(tzone, "UTC", 7) != 0)
  {
    // deal with optional sign
    auto sign = 1;
    char const* tzstr = &tzone[0];
    if (tzone[0] == '+')
      tzstr++;
    else if (tzone[0] == '-')
      tzstr++, sign = -1;

    auto len = strlen(tzstr);

    // deal with explicit colon character
    // scanfs are safe on the string view here since we know the string is null terminated
    bool okay = true;
    auto hh = 0, mm = 0;
    if (strchr(tzstr, ':') != nullptr)
      okay = sscanf(tzstr, "%2d:%2d", &hh, &mm) == 2;
    else if (len == 4)
      okay = sscanf(tzstr, "%2d%2d", &hh, &mm) == 2;
    else if (len == 3)
      okay = sscanf(tzstr, "%1d%2d", &hh, &mm) == 2;
    else if (len == 2)
      okay = sscanf(tzstr, "%2d", &hh) == 1;
    else if (len == 1)
      okay = sscanf(tzstr, "%1d", &hh) == 1;
    if (!okay)
      throw std::runtime_error{"Failed to parse iso8601 time"};

    tz_delta = sign * (hh * 3600 + mm * 60);
  }

  return std::chrono::system_clock::from_time_t(timegm(&tmm) - tz_delta);
}

static auto format_seconds_since_time(std::chrono::system_clock::time_point time) -> std::string
{
  auto tt = std::chrono::system_clock::to_time_t(time);

  struct tm tmm;
  if (gmtime_r(&tt, &tmm) == nullptr)
    throw std::runtime_error{"Failure in gmtime_r"};

  char buf[64];
  auto len = strftime(buf, sizeof(buf), "seconds since %Y-%m-%dT%H:%M:%SZ", &tmm);
  if (len <= 0)
    throw std::runtime_error{"Failure in strftime"};

  return std::string(buf);
}

static auto parse_seconds_since_time(std::string const& val) -> std::chrono::system_clock::time_point
{
  char t;
  char tzone[8];
  struct tm tmm;
  memset(&tmm, 0, sizeof(struct tm));
  tmm.tm_mon = 1;
  tmm.tm_mday = 1;

  auto ret = sscanf(
        val.c_str()
      , "seconds since %4d-%2d-%2d%c%2d:%2d:%2d%7s"
      , &tmm.tm_year
      , &tmm.tm_mon
      , &tmm.tm_mday
      , &t
      , &tmm.tm_hour
      , &tmm.tm_min
      , &tmm.tm_sec
      , tzone);
  if (ret < 3 || ret == 4 || ret == 5 || (ret >= 6 && t != ' ' && t != 'T'))
    throw std::runtime_error{"Failed to parse 'seconds since time' units"};

  // adjust tm structure offsets
  tmm.tm_year -= 1900;
  tmm.tm_mon -= 1;

  // determine the time zone adjustment
  /* CF allows a nightmarish number of different combinations for the timezone: signed or unsigned,
   * with a separating colon, and 1, 2, 3 or 4 digit representations without a colon. */
  auto tz_delta = 0;
  if (ret == 8 && strncmp(tzone, "Z", 7) != 0 && strncmp(tzone, "UTC", 7) != 0)
  {
    // deal with optional sign
    auto sign = 1;
    char const* tzstr = &tzone[0];
    if (tzone[0] == '+')
      tzstr++;
    else if (tzone[0] == '-')
      tzstr++, sign = -1;

    auto len = strlen(tzstr);

    // deal with explicit colon character
    // scanfs are safe on the string view here since we know the string is null terminated
    bool okay = true;
    auto hh = 0, mm = 0;
    if (strchr(tzstr, ':') != nullptr)
      okay = sscanf(tzstr, "%2d:%2d", &hh, &mm) == 2;
    else if (len == 4)
      okay = sscanf(tzstr, "%2d%2d", &hh, &mm) == 2;
    else if (len == 3)
      okay = sscanf(tzstr, "%1d%2d", &hh, &mm) == 2;
    else if (len == 2)
      okay = sscanf(tzstr, "%2d", &hh) == 1;
    else if (len == 1)
      okay = sscanf(tzstr, "%1d", &hh) == 1;
    if (!okay)
      throw std::runtime_error{"Failed to parse 'seconds since time' units"};

    tz_delta = sign * (hh * 3600 + mm * 60);
  }

  return std::chrono::system_clock::from_time_t(timegm(&tmm) - tz_delta);
}

dataset::dataset(sweep const& parent, int varid)
  : ncid_{parent.ncid_}
  , varid_{varid}
  , size_{parent.rays_ * parent.bins_}
{
  int ndims;
  if (auto status = nc_inq_varndims(ncid_, varid_, &ndims))
    throw error{"Failed to open dataset", status, ncid_, varid_};
  if (ndims != 2)
    throw error{"Failed to open dataset", "Unexpected rank", ncid_, varid_};

  int dimids[2];
  if (auto status = nc_inq_vardimid(ncid_, varid_, dimids))
    throw error{"Failed to open dataset", status, ncid_, varid_};
  if (dimids[0] != parent.dimid_ray_ || dimids[1] != parent.dimid_bin_)
    throw error{"Failed to open dataset", "Incorrect dimensions", ncid_, varid_};
}

dataset::dataset(sweep const& parent, char const* name, data_type type, int compression)
  : ncid_{parent.ncid_}
  , size_{parent.rays_ * parent.bins_}
{
  int dimids[2] = { parent.dimid_ray_, parent.dimid_bin_ };
  if (auto status = nc_def_var(ncid_, name, data_type_to_nc_type(type), 2, dimids, &varid_))
    throw error{"Failed to create dataset variable", status, ncid_, NC_GLOBAL, name};

  size_t dimlens[2] = { size_t(parent.rays_), size_t(parent.bins_) };
  if (auto status = nc_def_var_chunking(ncid_, varid_, NC_CHUNKED, dimlens))
    throw error{"Failed to setup cunking for variable", status, ncid_, NC_GLOBAL, name};
  if (auto status = nc_def_var_deflate(ncid_, varid_, 0, 1, compression))
    throw error{"Failed to setup compression for variable", status, ncid_, NC_GLOBAL, name};
}

dataset::dataset(sweep const& parent, standard_quantity quantity, data_type type, int compression)
  : dataset{parent, standard_quantity_traits(quantity).name, type, compression}
{
  auto const& meta = standard_quantity_traits(quantity);
  att_set(ncid_, varid_, "standard_name", meta.standard_name);
  att_set(ncid_, varid_, "long_name", meta.long_name);
  if (meta.units[0] != '\0')
    att_set(ncid_, varid_, "units", meta.units);
}

inline auto dataset::check_size_mismatch(size_t size) const -> void
{
  if (size != size_)
    throw error{"Data size mismatch", nullptr, ncid_, varid_};
}

auto dataset::name() const -> std::string
{
  char buf[NC_MAX_NAME];
  if (auto status = nc_inq_varname(ncid_, varid_, buf))
    throw error{"Failed to determine dataset name", status, ncid_, varid_};
  return buf;
}

auto dataset::type() const -> data_type
{
  nc_type type;
  if (auto status = nc_inq_vartype(ncid_, varid_, &type))
    throw error{"Failed to determine dataset type", status, ncid_, varid_};

  switch (type)
  {
  case NC_BYTE:
    return data_type::i8;
  case NC_UBYTE:
    return data_type::u8;
  case NC_SHORT:
    return data_type::i16;
  case NC_USHORT:
    return data_type::u16;
  case NC_INT:
    return data_type::i32;
  case NC_UINT:
    return data_type::u32;
  case NC_INT64:
    return data_type::i64;
  case NC_UINT64:
    return data_type::u64;
  case NC_FLOAT:
    return data_type::f32;
  case NC_DOUBLE:
    return data_type::f64;
  default:
    throw error{"Invalid dataset type", nullptr, ncid_, varid_, lookup_type_name(ncid_, type).c_str()};
  }
}

auto dataset::attribute_exists(char const* name) const -> bool
{
  return att_exists(ncid_, varid_, name);
}

auto dataset::attribute_get(char const* name) const -> std::optional<metadata_value>
{
  return att_get<metadata_value>(ncid_, varid_, name);
}

auto dataset::attribute_set(char const* name, metadata_value const& val) -> void
{
  std::visit([&](auto const& v)
  {
    using T = std::decay_t<decltype(v)>;
    if constexpr (is_specialization<T, std::vector>::value)
      att_set(ncid_, varid_, name, v.data(), v.size());
    else
      att_set(ncid_, varid_, name, v);
  }, val);
}

auto dataset::attribute_erase(char const* name) -> void
{
  att_erase(ncid_, varid_, name);
}

auto dataset::standard_name() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, varid_, "standard_name");
}

auto dataset::set_standard_name(std::string const& val) -> void
{
  att_set(ncid_, varid_, "standard_name", val);
}

auto dataset::long_name() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, varid_, "long_name");
}

auto dataset::set_long_name(std::string const& val) -> void
{
  att_set(ncid_, varid_, "long_name", val);
}

auto dataset::units() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, varid_, "units");
}

auto dataset::set_units(std::string const& val) -> void
{
  att_set(ncid_, varid_, "units", val);
}

auto dataset::fill_value() const -> std::optional<data_type_scalar>
{
  auto val = att_get<data_type_scalar>(ncid_, varid_, "_FillValue");
  if (val && val->index() != static_cast<size_t>(type()))
    throw error{"Mismatch between dataset type and attribute", nullptr, ncid_, varid_, "_FillValue"};
  return val;
}

auto dataset::set_fill_value(data_type_scalar val) -> void
{
  if (val.index() != static_cast<size_t>(type()))
    throw error{"Mismatch between dataset type and attribute", nullptr, ncid_, varid_, "_FillValue"};
  std::visit([&](auto const& v) { att_set(ncid_, varid_, "_FillValue", v); }, val);
}

auto dataset::undetect() const -> std::optional<data_type_scalar>
{
  auto val = att_get<data_type_scalar>(ncid_, varid_, "_Undetect");
  if (val && val->index() != static_cast<size_t>(type()))
    throw error{"Mismatch between dataset type and attribute", nullptr, ncid_, varid_, "_Undetect"};
  return val;
}

auto dataset::set_undetect(data_type_scalar val) -> void
{
  if (val.index() != static_cast<size_t>(type()))
    throw error{"Mismatch between dataset type and attribute", nullptr, ncid_, varid_, "_Undetect"};
  std::visit([&](auto const& v) { att_set(ncid_, varid_, "_Undetect", v); }, val);
}

auto dataset::scale_factor() const -> std::optional<float>
{
  return att_get<float>(ncid_, varid_, "scale_factor");
}

auto dataset::set_scale_factor(float val) -> void
{
  att_set(ncid_, varid_, "scale_factor", val);
}

auto dataset::add_offset() const -> std::optional<float>
{
  return att_get<float>(ncid_, varid_, "add_offset");
}

auto dataset::set_add_offset(float val) -> void
{
  att_set(ncid_, varid_, "add_offset", val);
}

auto dataset::coordinates() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, varid_, "coordinates");
}

auto dataset::set_coordinates(std::string const& val) -> void
{
  att_set(ncid_, varid_, "coordinates", val);
}

auto dataset::sampling_ratio() const -> std::optional<float>
{
  return att_get<float>(ncid_, varid_, "sampling_ratio");
}

auto dataset::set_sampling_ratio(float val) -> void
{
  att_set(ncid_, varid_, "sampling_ratio", val);
}

auto dataset::is_discrete() const -> std::optional<bool>
{
  return parse_bool(att_get<std::string>(ncid_, varid_, "is_discrete"));
}

auto dataset::set_is_discrete(bool val) -> void
{
  att_set(ncid_, varid_, "is_discrete", format_bool(val));
}

auto dataset::field_folds() const -> std::optional<bool>
{
  return parse_bool(att_get<std::string>(ncid_, varid_, "field_folds"));
}

auto dataset::set_field_folds(bool val) -> void
{
  att_set(ncid_, varid_, "field_folds", format_bool(val));
}

auto dataset::fold_limit_lower() const -> std::optional<float>
{
  return att_get<float>(ncid_, varid_, "fold_limit_lower");
}

auto dataset::set_fold_limit_lower(float val) -> void
{
  att_set(ncid_, varid_, "fold_limit_lower", val);
}

auto dataset::fold_limit_upper() const -> std::optional<float>
{
  return att_get<float>(ncid_, varid_, "fold_limit_upper");
}

auto dataset::set_fold_limit_upper(float val) -> void
{
  att_set(ncid_, varid_, "fold_limit_upper", val);
}

auto dataset::is_quality_field() const -> std::optional<bool>
{
  return parse_bool(att_get<std::string>(ncid_, varid_, "is_quality_field"));
}

auto dataset::set_is_quality_field(bool val) -> void
{
  att_set(ncid_, varid_, "is_quality_field", format_bool(val));
}

auto dataset::flag_values() const -> std::optional<data_type_vector>
{
  return att_get<data_type_vector>(ncid_, varid_, "flag_values");
}

auto dataset::set_flag_values(data_type_vector const& val) -> void
{
  if (val.index() != static_cast<size_t>(type()))
    throw error{"Mismatch between dataset type and attribute", nullptr, ncid_, varid_, "flag_values"};
  std::visit([&](auto const& v) { att_set(ncid_, varid_, "flag_values", v.data(), v.size()); }, val);
}

auto dataset::flag_meanings() const -> std::optional<std::vector<std::string>>
{
  return att_get<std::vector<std::string>>(ncid_, varid_, "flag_meanings");
}

auto dataset::set_flag_meanings(std::vector<std::string> const& val) -> void
{
  att_set(ncid_, varid_, "flag_meanings", val.data(), val.size());
}

auto dataset::flag_masks() const -> std::optional<data_type_vector>
{
  return att_get<data_type_vector>(ncid_, varid_, "flag_masks");
}

auto dataset::set_flag_masks(data_type_vector const& val) -> void
{
  if (val.index() != static_cast<size_t>(type()))
    throw error{"Mismatch between dataset type and attribute", nullptr, ncid_, varid_, "flag_masks"};
  std::visit([&](auto const& v) { att_set(ncid_, varid_, "flag_masks", v.data(), v.size()); }, val);
}

auto dataset::qualified_variables() const -> std::optional<std::vector<std::string>>
{
  return att_get<std::vector<std::string>>(ncid_, varid_, "qualified_variables");
}

auto dataset::set_qualified_variables(std::vector<std::string> const& val) -> void
{
  att_set(ncid_, varid_, "qualified_variables", val.data(), val.size());
}

auto dataset::ancillary_variables() const -> std::optional<std::vector<std::string>>
{
  return att_get<std::vector<std::string>>(ncid_, varid_, "ancillary_variables");
}

auto dataset::set_ancillary_variables(std::vector<std::string> const& val) -> void
{
  att_set(ncid_, varid_, "ancillary_variables", val.data(), val.size());
}

auto dataset::thresholding_xml() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, varid_, "thresholding_xml");
}

auto dataset::set_thresholding_xml(std::string const& val) -> void
{
  att_set(ncid_, varid_, "thresholding_xml", val);
}

auto dataset::legend_xml() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, varid_, "legend_xml");
}

auto dataset::set_legend_xml(std::string const& val) -> void
{
  att_set(ncid_, varid_, "legend_xml", val);
}

auto dataset::read_raw(std::span<int8_t> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<uint8_t> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<int16_t> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<uint16_t> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<int32_t> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<uint32_t> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<int64_t> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<uint64_t> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<float> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::read_raw(std::span<double> data) const -> void
{
  check_size_mismatch(data.size());
  wrap_nc_get_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<int8_t const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<uint8_t const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<int16_t const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<uint16_t const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<int32_t const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<uint32_t const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<int64_t const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<uint64_t const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<float const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto dataset::write_raw(std::span<double const> data) -> void
{
  check_size_mismatch(data.size());
  wrap_nc_put_var(ncid_, varid_, data.data());
}

auto group::attribute_exists(char const* name) const -> bool
{
  return att_exists(ncid_, NC_GLOBAL, name);
}

auto group::attribute_get(char const* name) const -> std::optional<metadata_value>
{
  return att_get<metadata_value>(ncid_, NC_GLOBAL, name);
}

auto group::attribute_set(char const* name, metadata_value const& val) -> void
{
  std::visit([&](auto const& v)
  {
    using T = std::decay_t<decltype(v)>;
    if constexpr (is_specialization<T, std::vector>::value)
      att_set(ncid_, NC_GLOBAL, name, v.data(), v.size());
    else
      att_set(ncid_, NC_GLOBAL, name, v);
  }, val);
}

auto group::attribute_erase(char const* name) -> void
{
  att_erase(ncid_, NC_GLOBAL, name);
}

auto group::variable_exists(char const* name) const -> bool
{
  return var_exists(ncid_, name);
}

auto group::variable_get(char const* name) const -> std::optional<metadata_scalar>
{
  return var_get<metadata_scalar>(ncid_, name);
}

auto group::variable_set(char const* name, metadata_scalar const& val) -> void
{
  std::visit([&](auto const& v){ var_set(ncid_, name, v); }, val);
}

auto georeference_group::latitude() -> std::optional<std::vector<double>>
{
  return var_get<std::vector<double>>(ncid_, "latitude", dimid_ray_);
}

auto georeference_group::set_latitude(std::vector<double> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "latitude", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees_north");
}

auto georeference_group::longitude() -> std::optional<std::vector<double>>
{
  return var_get<std::vector<double>>(ncid_, "longitude", dimid_ray_);
}

auto georeference_group::set_longitude(std::vector<double> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "longitude", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees_east");
}

auto georeference_group::altitude() -> std::optional<std::vector<double>>
{
  return var_get<std::vector<double>>(ncid_, "altitude", dimid_ray_);
}

auto georeference_group::set_altitude(std::vector<double> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "altitude", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "meters");
}

auto georeference_group::heading() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "heading", dimid_ray_);
}

auto georeference_group::set_heading(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "heading", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_group::roll() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "roll", dimid_ray_);
}

auto georeference_group::set_roll(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "roll", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_group::pitch() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "pitch", dimid_ray_);
}

auto georeference_group::set_pitch(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "pitch", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_group::drift() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "drift", dimid_ray_);
}

auto georeference_group::set_drift(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "drift", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_group::rotation() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "rotation", dimid_ray_);
}

auto georeference_group::set_rotation(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "rotation", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_group::tilt() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "tilt", dimid_ray_);
}

auto georeference_group::set_tilt(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "tilt", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_group::eastward_velocity() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "eastward_velocity", dimid_ray_);
}

auto georeference_group::set_eastward_velocity(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "eastward_velocity", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_group::northward_velocity() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "northward_velocity", dimid_ray_);
}

auto georeference_group::set_northward_velocity(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "northward_velocity", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_group::vertical_velocity() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "vertical_velocity", dimid_ray_);
}

auto georeference_group::set_vertical_velocity(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "vertical_velocity", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_group::eastward_wind() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "eastward_wind", dimid_ray_);
}

auto georeference_group::set_eastward_wind(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "eastward_wind", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_group::northward_wind() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "northward_wind", dimid_ray_);
}

auto georeference_group::set_northward_wind(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "northward_wind", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_group::vertical_wind() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "vertical_wind", dimid_ray_);
}

auto georeference_group::set_vertical_wind(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "vertical_wind", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_group::heading_rate() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "heading_rate", dimid_ray_);
}

auto georeference_group::set_heading_rate(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "heading_rate", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees/s");
}

auto georeference_group::roll_rate() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "roll_rate", dimid_ray_);
}

auto georeference_group::set_roll_rate(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "roll_rate", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees/s");
}

auto georeference_group::pitch_rate() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "pitch_rate", dimid_ray_);
}

auto georeference_group::set_pitch_rate(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "pitch_rate", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees/s");
}

auto georeference_group::georefs_applied() -> std::optional<std::vector<uint8_t>>
{
  return var_get<std::vector<uint8_t>>(ncid_, "georefs_applied", dimid_ray_);
}

auto georeference_group::set_georefs_applied(std::vector<uint8_t> const& val) -> void
{
  var_set(ncid_, "georefs_applied", dimid_ray_, val);
}

auto monitoring_group::measured_transmit_power_h() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "measured_transmit_power_h", dimid_ray_);
}

auto monitoring_group::set_measured_transmit_power_h(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "measured_transmit_power_h", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto monitoring_group::measured_transmit_power_v() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "measured_transmit_power_v", dimid_ray_);
}

auto monitoring_group::set_measured_transmit_power_v(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "measured_transmit_power_v", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto monitoring_group::measured_sky_noise() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "measured_sky_noise", dimid_ray_);
}

auto monitoring_group::set_measured_sky_noise(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "measured_sky_noise", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto monitoring_group::measured_cold_noise() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "measured_cold_noise", dimid_ray_);
}

auto monitoring_group::set_measured_cold_noise(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "measured_cold_noise", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto monitoring_group::measured_hot_noise() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "measured_hot_noise", dimid_ray_);
}

auto monitoring_group::set_measured_hot_noise(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "measured_hot_noise", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto monitoring_group::phase_difference_transmit_hv() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "phase_difference_transmit_hv", dimid_ray_);
}

auto monitoring_group::set_phase_difference_transmit_hv(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "phase_difference_transmit_hv", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto monitoring_group::antenna_pointing_accuracy_elev() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "antenna_pointing_accuracy_elev", dimid_ray_);
}

auto monitoring_group::set_antenna_pointing_accuracy_elev(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "antenna_pointing_accuracy_elev", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto monitoring_group::antenna_pointing_accuracy_az() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "antenna_pointing_accuracy_az", dimid_ray_);
}

auto monitoring_group::set_antenna_pointing_accuracy_az(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "antenna_pointing_accuracy_az", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto monitoring_group::calibration_offset_h() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "calibration_offset_h", dimid_ray_);
}

auto monitoring_group::set_calibration_offset_h(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "calibration_offset_h", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto monitoring_group::calibration_offset_v() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "calibration_offset_v", dimid_ray_);
}

auto monitoring_group::set_calibration_offset_v(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "calibration_offset_v", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto monitoring_group::zdr_offset() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "zdr_offset", dimid_ray_);
}

auto monitoring_group::set_zdr_offset(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "zdr_offset", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

sweep::sweep(int ncid)
  : group{ncid}
  , dimid_ray_{wrap_nc_inq_dimid(ncid_, "time")}
  , dimid_bin_{wrap_nc_inq_dimid(ncid_, "range")}
  , dimid_freq_{wrap_nc_inq_dimid(ncid_, "frequency")}
  , dimid_prt_{dim_exists(ncid_, "prt") ? wrap_nc_inq_dimid(ncid_, "prt") : -1}
  , rays_{wrap_nc_inq_dimlen(ncid_, dimid_ray_)}
  , bins_{wrap_nc_inq_dimlen(ncid_, dimid_bin_)}
  , freqs_{wrap_nc_inq_dimlen(ncid_, dimid_freq_)}
  , prts_{dimid_prt_ != -1 ? wrap_nc_inq_dimlen(ncid_, dimid_prt_) : 0}
{
  // scan the variables to determine the dataset count, we store the names for convenience too
  int nvars;
  if (auto status = nc_inq_nvars(ncid_, &nvars))
    throw error{"Failed to count dataset variables", status, ncid_};
  for (auto varid = 0; varid < nvars; ++varid)
  {
    // is it 2D?
    int ndims;
    if (auto status = nc_inq_varndims(ncid, varid, &ndims))
      throw error{"Failed to count dataset variables", status, ncid_};
    if (ndims != 2)
      continue;

    // are the dimensions 'time' and 'range'
    int dimids[2];
    if (auto status = nc_inq_vardimid(ncid, varid, dimids))
      throw error{"Failed to count dataset variables", status, ncid_};
    if (dimids[0] != dimid_ray_ || dimids[1] != dimid_bin_)
      continue;

    // store the variable name
    char name[NC_MAX_NAME];
    if (auto status = nc_inq_varname(ncid, varid, name))
      throw error{"Failed to count dataset variables", status, ncid_};
    datasets_.emplace_back(name);
  }
}

sweep::sweep(int ncid, size_t rays, size_t bins, size_t freqs, size_t prts)
  : group{ncid}
  , dimid_ray_{wrap_nc_def_dim(ncid_, "time", rays)}
  , dimid_bin_{wrap_nc_def_dim(ncid_, "range", bins)}
  , dimid_freq_{wrap_nc_def_dim(ncid_, "frequency", freqs)}
  , dimid_prt_{prts > 0 ? wrap_nc_def_dim(ncid_, "prt", prts) : -1}
  , rays_{rays}
  , bins_{bins}
  , freqs_{freqs}
  , prts_{prts}
{ }

auto sweep::ray_variable_get(char const* name) const -> std::optional<metadata_vector>
{
  return var_get<metadata_vector>(ncid_, name, dimid_ray_);
}

auto sweep::ray_variable_set(char const* name, metadata_vector const& val) -> void
{
  std::visit([&](auto const& v){ var_set(ncid_, name, dimid_ray_, v); }, val);
}

auto sweep::ray_time() -> std::optional<std::pair<std::vector<std::chrono::duration<double>>, std::chrono::system_clock::time_point>>
{
  auto raw_val = var_get<std::vector<double>>(ncid_, "time", dimid_ray_);
  if (!raw_val)
    return std::nullopt;

  auto varid = wrap_nc_inq_varid(ncid_, "time");
  auto units = att_get<std::string>(ncid_, varid, "units");
  if (!units)
    throw error{"Failed to read attribute", NC_NOERR, ncid_, varid, "units"};
  auto since_time = parse_seconds_since_time(*units);

  auto val = std::vector<std::chrono::duration<double>>(raw_val->size());
  for (size_t i = 0; i < raw_val->size(); ++i)
    val[i] = std::chrono::duration<double>{(*raw_val)[i]};

  return std::pair<std::vector<std::chrono::duration<double>>, std::chrono::system_clock::time_point>{std::move(val), since_time};
}

auto sweep::ray_set_time(std::vector<std::chrono::duration<double>> const& val, std::chrono::system_clock::time_point since_time) -> void
{
  auto raw_val = std::vector<double>(val.size());
  for (size_t i = 0; i < val.size(); ++i)
    raw_val[i] = val[i].count();

  if (auto [varid, isnew] = var_set(ncid_, "time", dimid_ray_, raw_val); isnew)
  {
    att_set(ncid_, varid, "standard_name", "time");
    att_set(ncid_, varid, "units", format_seconds_since_time(since_time));
  }
}

auto sweep::ray_azimuth() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "azimuth", dimid_ray_);
}

auto sweep::ray_set_azimuth(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "azimuth", dimid_ray_, val); isnew)
  {
    att_set(ncid_, varid, "standard_name", "sensor_to_target_azimuth_angle");
    att_set(ncid_, varid, "units", "degrees");
    att_set(ncid_, varid, "long_name", "Azimuth angle from true north");
    att_set(ncid_, varid, "axis", "radial_azimuth_coordinate");
  }
}

auto sweep::ray_elevation() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "elevation", dimid_ray_);
}

auto sweep::ray_set_elevation(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "elevation", dimid_ray_, val); isnew)
  {
    att_set(ncid_, varid, "standard_name", "sensor_to_target_elevation_angle");
    att_set(ncid_, varid, "units", "degrees");
    att_set(ncid_, varid, "long_name", "Elevation angle from horizontal plane");
    att_set(ncid_, varid, "axis", "radial_elevation_coordinate");
  }
}

auto sweep::ray_scan_rate() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "scan_rate", dimid_ray_);
}

auto sweep::ray_set_scan_rate(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "scan_rate", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "degrees/s");
}

auto sweep::ray_antenna_transition() -> std::optional<std::vector<uint8_t>>
{
  return var_get<std::vector<uint8_t>>(ncid_, "antenna_transition", dimid_ray_);
}

auto sweep::ray_set_antenna_transition(std::vector<uint8_t> const& val) -> void
{
  var_set(ncid_, "antenna_transition", dimid_ray_, val);
}

auto sweep::ray_pulse_width() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "pulse_width", dimid_ray_);
}

auto sweep::ray_set_pulse_width(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "pulse_width", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "seconds");
}

auto sweep::ray_calib_index() -> std::optional<std::vector<int>>
{
  return var_get<std::vector<int>>(ncid_, "calib_index", dimid_ray_);
}

auto sweep::ray_set_calib_index(std::vector<int> const& val) -> void
{
  var_set(ncid_, "calib_index", dimid_ray_, val);
}

auto sweep::ray_rx_range_resolution() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "rx_range_resolution", dimid_ray_);
}

auto sweep::ray_set_rx_range_resolution(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "rx_range_resolution", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "meters");
}

auto sweep::ray_prt() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "prt", dimid_ray_);
}

auto sweep::ray_set_prt(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "prt", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "seconds");
}

auto sweep::ray_prt_ratio() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "prt_ratio", dimid_ray_);
}

auto sweep::ray_set_prt_ratio(std::vector<float> const& val) -> void
{
  var_set(ncid_, "prt_ratio", dimid_ray_, val);
}

auto sweep::ray_prt_sequence() -> std::optional<std::vector<float>>
{
  /* this is a special case of a 2d metadata field, which is a real pain given that its the only one defined for the
   * entire cfradial 2 spec.  unlike for each of the other dimensions, we do not provide the user with any mechanism
   * to read or write similarly dimensioned 2d 'generic' or user defined metadata.  users who really want to do that
   * need to grab the ncid and do it themselves */
  auto name = "prt_sequence";
  int varid;
  if (auto status = nc_inq_varid(ncid_, name, &varid))
  {
    if (status != NC_ENOTVAR)
      throw error{"Failed to read variable", status, ncid_, NC_GLOBAL, name};
    return std::nullopt;
  }

  int ndims;
  if (auto status = nc_inq_varndims(ncid_, varid, &ndims))
    throw error{"Failed to read variable", status, ncid_, NC_GLOBAL, name};
  if (ndims != 2)
    throw error{"Failed to read variable", "2D variable expected", ncid_, NC_GLOBAL, name};

  int found_dimids[2];
  if (auto status = nc_inq_vardimid(ncid_, varid, found_dimids))
    throw error{"Failed to read variable", status, ncid_, NC_GLOBAL, name};
  if (found_dimids[0] != dimid_ray_ || found_dimids[1] != dimid_prt_)
    throw error{"Failed to read variable", "Unexpected variable dimension", ncid_, NC_GLOBAL, name};

  std::optional<std::vector<float>> val{std::in_place, rays_ * prts_};
  wrap_nc_get_var(ncid_, varid, val->data());
  return val;
}

auto sweep::ray_set_prt_sequence(std::vector<float> const& val) -> void
{
  /* this is a special case of a 2d metadata field, which is a real pain given that its the only one defined for the
   * entire cfradial 2 spec.  unlike for each of the other dimensions, we do not provide the user with any mechanism
   * to read or write similarly dimensioned 2d 'generic' or user defined metadata.  users who really want to do that
   * need to grab the ncid and do it themselves */
  auto name = "prt_sequence";
  int varid;
  if (auto status = nc_inq_varid(ncid_, name, &varid))
  {
    if (status != NC_ENOTVAR)
      throw error{"Failed to write variable", status, ncid_, NC_GLOBAL, name};
    int dimids[2];
    dimids[0] = dimid_ray_;
    dimids[1] = dimid_prt_;
    if (auto status = nc_def_var(ncid_, name, native_to_nc_type<float>(), 2, dimids, &varid))
      throw error{"Failed to create variable", status, ncid_, NC_GLOBAL, name};

    att_set(ncid_, varid, "units", "seconds");
  }
  else
  {
    int ndims;
    if (auto status = nc_inq_varndims(ncid_, varid, &ndims))
      throw error{"Failed to read variable", status, ncid_, NC_GLOBAL, name};
    if (ndims != 2)
      throw error{"Failed to read variable", "2D variable expected", ncid_, NC_GLOBAL, name};

    int found_dimids[2];
    if (auto status = nc_inq_vardimid(ncid_, varid, found_dimids))
      throw error{"Failed to read variable", status, ncid_, NC_GLOBAL, name};
    if (found_dimids[0] != dimid_ray_ || found_dimids[1] != dimid_prt_)
      throw error{"Failed to read variable", "Unexpected variable dimension", ncid_, NC_GLOBAL, name};
  }

  wrap_nc_put_var(ncid_, varid, val.data());
}

auto sweep::ray_nyquist_velocity() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "nyquist_velocity", dimid_ray_);
}

auto sweep::ray_set_nyquist_velocity(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "nyquist_velocity", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "meters/s");
}

auto sweep::ray_unambiguous_range() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "unambiguous_range", dimid_ray_);
}

auto sweep::ray_set_unambiguous_range(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "unambiguous_range", dimid_ray_, val); isnew)
    att_set(ncid_, varid, "units", "meters");
}

auto sweep::ray_n_samples() -> std::optional<std::vector<int>>
{
  return var_get<std::vector<int>>(ncid_, "n_samples", dimid_ray_);
}

auto sweep::ray_set_n_samples(std::vector<int> const& val) -> void
{
  var_set(ncid_, "n_samples", dimid_ray_, val);
}

auto sweep::bin_variable_get(char const* name) const -> std::optional<metadata_vector>
{
  return var_get<metadata_vector>(ncid_, name, dimid_bin_);
}

auto sweep::bin_variable_set(char const* name, metadata_vector const& val) -> void
{
  std::visit([&](auto const& v){ var_set(ncid_, name, dimid_bin_, v); }, val);
}

auto sweep::bin_range() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "range", dimid_bin_);
}

auto sweep::bin_set_range(std::vector<float> const& val) -> void
{
  auto [varid, isnew] = var_set(ncid_, "range", dimid_bin_, val);
  if (isnew)
  {
    att_set(ncid_, varid, "standard_name", "projection_range_coordinate");
    att_set(ncid_, varid, "long_name", "range_to_measurement_volume");
    att_set(ncid_, varid, "units", "meters");
    att_set(ncid_, varid, "axis", "radial_range_coordinate");
  }
  if (!val.empty())
    att_set(ncid_, varid, "meters_to_center_of_first_gate", val[0]);

  // check if ranges are constantly spaced
  bool constant_spaced = true;
  if (val.size() > 1)
  {
    auto delta = val[1] - val[0];
    for (size_t i = 2; i < val.size(); ++i)
    {
      if (std::abs((val[i] - val[i-1]) - delta) > 0.001f)
      {
        constant_spaced = false;
        break;
      }
    }
    if (constant_spaced)
      att_set(ncid_, varid, "meters_between_gates", delta);
  }
  att_set(ncid_, varid, "spacing_is_constant", constant_spaced);
}

auto sweep::frequency_variable_get(char const* name) const -> std::optional<metadata_vector>
{
  return var_get<metadata_vector>(ncid_, name, dimid_freq_);
}

auto sweep::frequency_variable_set(char const* name, metadata_vector const& val) -> void
{
  std::visit([&](auto const& v){ var_set(ncid_, name, dimid_freq_, v); }, val);
}

auto sweep::frequency_frequency() -> std::optional<std::vector<float>>
{
  return var_get<std::vector<float>>(ncid_, "frequency", dimid_freq_);
}

auto sweep::frequency_set_frequency(std::vector<float> const& val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "frequency", dimid_freq_, val); isnew)
  {
    att_set(ncid_, varid, "standard_name", "radiation_frequency");
    att_set(ncid_, varid, "long_name", "Operating frequency");
    att_set(ncid_, varid, "units", "s-1");
  }
}

auto sweep::prt_variable_get(char const* name) const -> std::optional<metadata_vector>
{
  return var_get<metadata_vector>(ncid_, name, dimid_prt_);
}

auto sweep::prt_variable_set(char const* name, metadata_vector const& val) -> void
{
  std::visit([&](auto const& v){ var_set(ncid_, name, dimid_prt_, v); }, val);
}

auto sweep::dataset_exists(char const* name) const -> bool
{
  return std::find(datasets_.begin(), datasets_.end(), name) != datasets_.end();
}

auto sweep::dataset(char const* name) -> std::optional<fm301::dataset>
{
  int varid;
  if (auto status = nc_inq_varid(ncid_, name, &varid))
  {
    if (status == NC_ENOTVAR)
      return std::nullopt;
    else
      throw error{"Failed to find dataset variable", status, ncid_, NC_GLOBAL, name};
  }
  return fm301::dataset{*this, varid};
}

auto sweep::dataset(char const* name) const -> std::optional<fm301::dataset const>
{
  int varid;
  if (auto status = nc_inq_varid(ncid_, name, &varid))
  {
    if (status == NC_ENOTVAR)
      return std::nullopt;
    else
      throw error{"Failed to find dataset variable", status, ncid_, NC_GLOBAL, name};
  }
  return fm301::dataset{*this, varid};
}

auto sweep::dataset(standard_quantity quantity) -> std::optional<fm301::dataset>
{
  return dataset(standard_quantity_traits(quantity).name);
}

auto sweep::dataset_add(char const* name, data_type type, int compression) -> fm301::dataset
{
  return {*this, name, type, compression};
}

auto sweep::dataset_add(standard_quantity quantity, data_type type, int compression) -> fm301::dataset
{
  return {*this, quantity, type, compression};
}

auto sweep::georeference() const -> std::optional<georeference_group>
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "georeference", &grpid))
    return std::nullopt;
  return georeference_group{grpid, dimid_ray_};
}

auto sweep::georeference_add() -> georeference_group
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "georeference", &grpid))
    return georeference_group{wrap_nc_def_grp(ncid_, "georeference"), dimid_ray_};
  return georeference_group{grpid, dimid_ray_};
}

auto sweep::monitoring() const -> std::optional<monitoring_group>
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "monitoring", &grpid))
    return std::nullopt;
  return monitoring_group{grpid, dimid_ray_};
}

auto sweep::monitoring_add() -> monitoring_group
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "monitoring", &grpid))
    return monitoring_group{wrap_nc_def_grp(ncid_, "monitoring"), dimid_ray_};
  return monitoring_group{grpid, dimid_ray_};
}

auto sweep::sweep_number() const -> std::optional<int>
{
  return var_get<int>(ncid_, "sweep_number");
}

auto sweep::set_sweep_number(int val) -> void
{
  var_set(ncid_, "sweep_number", val);
}

auto sweep::sweep_mode() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "sweep_mode");
}

auto sweep::set_sweep_mode(std::string const& val) -> void
{
  var_set(ncid_, "sweep_mode", val);
}

auto sweep::follow_mode() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "follow_mode");
}

auto sweep::set_follow_mode(std::string const& val) -> void
{
  var_set(ncid_, "follow_mode", val);
}

auto sweep::prt_mode() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "prt_mode");
}

auto sweep::set_prt_mode(std::string const& val) -> void
{
  var_set(ncid_, "prt_mode", val);
}

auto sweep::fixed_angle() const -> std::optional<float>
{
  return var_get<float>(ncid_, "fixed_angle");
}

auto sweep::set_fixed_angle(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "fixed_angle", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto sweep::polarization_mode() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "polarization_mode");
}

auto sweep::set_polarization_mode(std::string const& val) -> void
{
  var_set(ncid_, "polarization_mode", val);
}

auto sweep::polarization_sequence() const -> std::optional<std::vector<std::string>>
{
  return var_get<std::vector<std::string>>(ncid_, "polarization_sequence", dimid_prt_);
}

auto sweep::set_polarization_sequence(std::vector<std::string> const& val) -> void
{
  var_set(ncid_, "polarization_sequence", dimid_prt_, val);
}

auto sweep::rays_are_indexed() const -> std::optional<bool>
{
  return parse_bool(var_get<std::string>(ncid_, "rays_are_indexed"));
}

auto sweep::set_rays_are_indexed(bool val) -> void
{
  var_set(ncid_, "rays_are_indexed", std::string(format_bool(val)));
}

auto sweep::ray_angle_resolution() const -> std::optional<float>
{
  return var_get<float>(ncid_, "ray_angle_resolution");
}

auto sweep::set_ray_angle_resolution(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "ray_angle_resolution", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto sweep::qc_procedures() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "qc_procedures");
}

auto sweep::set_qc_procedures(std::string const& val) -> void
{
  var_set(ncid_, "qc_procedures", val);
}

auto sweep::target_scan_rate() const -> std::optional<float>
{
  return var_get<float>(ncid_, "target_scan_rate");
}

auto sweep::set_target_scan_rate(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "target_scan_rate", val); isnew)
    att_set(ncid_, varid, "units", "degrees/s");
}

auto radar_parameters_group::antenna_gain_h() const -> std::optional<float>
{
  return var_get<float>(ncid_, "antenna_gain_h");
}

auto radar_parameters_group::set_antenna_gain_h(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "antenna_gain_h", val); isnew)
  {
    att_set(ncid_, varid, "units", "dBi");
    att_set(ncid_, varid, "long_name", "Nominal antenna gain, H polarization");
  }
}

auto radar_parameters_group::antenna_gain_v() const -> std::optional<float>
{
  return var_get<float>(ncid_, "antenna_gain_v");
}

auto radar_parameters_group::set_antenna_gain_v(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "antenna_gain_v", val); isnew)
  {
    att_set(ncid_, varid, "units", "dBi");
    att_set(ncid_, varid, "long_name", "Nominal antenna gain, V polarization");
  }
}

auto radar_parameters_group::beam_width_h() const -> std::optional<float>
{
  return var_get<float>(ncid_, "beam_width_h");
}

auto radar_parameters_group::set_beam_width_h(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "beam_width_h", val); isnew)
  {
    att_set(ncid_, varid, "units", "degrees");
    att_set(ncid_, varid, "long_name", "Antenna beam width, H polarization");
  }
}

auto radar_parameters_group::beam_width_v() const -> std::optional<float>
{
  return var_get<float>(ncid_, "beam_width_v");
}

auto radar_parameters_group::set_beam_width_v(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "beam_width_v", val); isnew)
  {
    att_set(ncid_, varid, "units", "degrees");
    att_set(ncid_, varid, "long_name", "Antenna beam width, V polarization");
  }
}

auto radar_parameters_group::receiver_bandwidth() const -> std::optional<float>
{
  return var_get<float>(ncid_, "receiver_bandwidth");
}

auto radar_parameters_group::set_receiver_bandwidth(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "receiver_bandwidth", val); isnew)
  {
    att_set(ncid_, varid, "units", "s-1");
    att_set(ncid_, varid, "long_name", "Bandwidth of radar receiver");
  }
}

auto lidar_parameters_group::beam_divergence() const -> std::optional<float>
{
  return var_get<float>(ncid_, "beam_divergence");
}

auto lidar_parameters_group::set_beam_divergence(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "beam_divergence", val); isnew)
  {
    att_set(ncid_, varid, "units", "milliradians");
    att_set(ncid_, varid, "comment", "Transmit side");
  }
}

auto lidar_parameters_group::field_of_view() const -> std::optional<float>
{
  return var_get<float>(ncid_, "field_of_view");
}

auto lidar_parameters_group::set_field_of_view(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "field_of_view", val); isnew)
  {
    att_set(ncid_, varid, "units", "milliradians");
    att_set(ncid_, varid, "comment", "Receive side");
  }
}

auto lidar_parameters_group::aperture_diameter() const -> std::optional<float>
{
  return var_get<float>(ncid_, "aperture_diameter");
}

auto lidar_parameters_group::set_aperture_diameter(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "aperture_diameter", val); isnew)
    att_set(ncid_, varid, "units", "cm");
}

auto lidar_parameters_group::aperture_efficiency() const -> std::optional<float>
{
  return var_get<float>(ncid_, "aperture_efficiency");
}

auto lidar_parameters_group::set_aperture_efficiency(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "aperture_efficiency", val); isnew)
    att_set(ncid_, varid, "units", "percent");
}

auto lidar_parameters_group::peak_power() const -> std::optional<float>
{
  return var_get<float>(ncid_, "peak_power");
}

auto lidar_parameters_group::set_peak_power(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "peak_power", val); isnew)
    att_set(ncid_, varid, "units", "watts");
}

auto lidar_parameters_group::pulse_energy() const -> std::optional<float>
{
  return var_get<float>(ncid_, "pulse_energy");
}

auto lidar_parameters_group::set_pulse_energy(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "pulse_energy", val); isnew)
    att_set(ncid_, varid, "units", "joules");
}

radar_calibration_group::radar_calibration_group(int ncid)
  : group{ncid}
  , dimid_calib_{wrap_nc_inq_dimid(ncid_, "calib")}
{ }

radar_calibration_group::radar_calibration_group(int ncid, size_t calibration_count)
  : group{ncid}
  , dimid_calib_{wrap_nc_def_dim(ncid_, "calib", calibration_count)}
{ }

auto radar_calibration_group::calibration_count() const -> size_t
{
  return wrap_nc_inq_dimlen(ncid_, dimid_calib_);
}

auto radar_calibration_group::calibration_variable_get(char const* name, size_t calib) const -> std::optional<metadata_scalar>
{
  return var_get1<metadata_scalar>(ncid_, name, dimid_calib_, calib);
}

auto radar_calibration_group::calibration_variable_set(char const* name, size_t calib, metadata_scalar const& val) -> void
{
  std::visit([&](auto const& v){ var_set1(ncid_, name, dimid_calib_, calib, v); }, val);
}

auto radar_calibration_group::calib_index(size_t calib) const -> std::optional<uint8_t>
{
  return var_get1<uint8_t>(ncid_, "calib_index", dimid_calib_, calib);
}

auto radar_calibration_group::set_calib_index(size_t calib, uint8_t val) -> void
{
  var_set1(ncid_, "calib_index", dimid_calib_, calib, val);
}

auto radar_calibration_group::time(size_t calib) const -> std::optional<std::chrono::system_clock::time_point>
{
  auto raw_val = var_get1<double>(ncid_, "time", dimid_calib_, calib);
  if (!raw_val)
    return std::nullopt;

  auto varid = wrap_nc_inq_varid(ncid_, "time");
  auto units = att_get<std::string>(ncid_, varid, "units");
  if (!units)
    throw error{"Failed to read attribute", NC_NOERR, ncid_, varid, "units"};
  auto since_time = parse_seconds_since_time(*units);

  auto val = std::chrono::duration<double>(*raw_val);

  return since_time + std::chrono::duration_cast<std::chrono::system_clock::duration>(val);
}

auto radar_calibration_group::set_time(size_t calib, std::chrono::system_clock::time_point val) -> void
{
  if (var_exists(ncid_, "time"))
  {
    // time variable already created so output value relative to existing "since" time
    auto varid = wrap_nc_inq_varid(ncid_, "time");
    auto units = att_get<std::string>(ncid_, varid, "units");
    if (!units)
      throw error{"Failed to read attribute", NC_NOERR, ncid_, varid, "units"};
    auto since_time = parse_seconds_since_time(*units);
    auto raw_val = std::chrono::duration_cast<std::chrono::duration<double>>(val - since_time).count();
    var_set1(ncid_, "time", dimid_calib_, calib, raw_val);
  }
  else
  {
    // time variable is new so write variable so use val as the "since" time
    auto [varid, isnew] = var_set1(ncid_, "time", dimid_calib_, calib, 0.0);
    att_set(ncid_, varid, "units", format_seconds_since_time(val));
  }
}

auto radar_calibration_group::pulse_width(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "pulse_width", dimid_calib_, calib);
}

auto radar_calibration_group::set_pulse_width(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "pulse_width", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "seconds");
}

auto radar_calibration_group::antenna_gain_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "antenna_gain_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_antenna_gain_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "antenna_gain_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::antenna_gain_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "antenna_gain_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_antenna_gain_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "antenna_gain_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::xmit_power_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "xmit_power_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_xmit_power_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "xmit_power_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::xmit_power_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "xmit_power_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_xmit_power_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "xmit_power_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::two_way_waveguide_loss_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "two_way_waveguide_loss_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_two_way_waveguide_loss_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "two_way_waveguide_loss_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::two_way_waveguide_loss_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "two_way_waveguid_loss_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_two_way_waveguide_loss_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "two_way_waveguid_loss_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::two_way_radome_loss_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "two_way_radome_loss_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_two_way_radome_loss_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "two_way_radome_loss_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::two_way_radome_loss_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "two_way_radome_loss_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_two_way_radome_loss_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "two_way_radome_loss_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::receiver_mismatch_loss(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_mismatch_loss", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_mismatch_loss(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "receiver_mismatch_loss", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::receiver_mismatch_loss_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_mismatch_loss_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_mismatch_loss_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "receiver_mismatch_loss_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::receiver_mismatch_loss_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_mismatch_loss_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_mismatch_loss_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "receiver_mismatch_loss_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::radar_constant_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "radar_constant_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_radar_constant_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "radar_constant_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB(m/mW)");
}

auto radar_calibration_group::radar_constant_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "radar_constant_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_radar_constant_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "radar_constant_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB(m/mW)");
}

auto radar_calibration_group::probert_jones_correction(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "probert_jones_correction", dimid_calib_, calib);
}

auto radar_calibration_group::set_probert_jones_correction(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "probert_jones_correction", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::dielectric_factor_used(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "dielectric_factor_used", dimid_calib_, calib);
}

auto radar_calibration_group::set_dielectric_factor_used(size_t calib, float val) -> void
{
  var_set1(ncid_, "dielectric_factor_used", dimid_calib_, calib, val);
}

auto radar_calibration_group::noise_hc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "noise_hc", dimid_calib_, calib);
}

auto radar_calibration_group::set_noise_hc(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "noise_hc", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::noise_vc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "noise_vc", dimid_calib_, calib);
}

auto radar_calibration_group::set_noise_vc(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "noise_vc", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::noise_hx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "noise_hx", dimid_calib_, calib);
}

auto radar_calibration_group::set_noise_hx(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "noise_hx", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::noise_vx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "noise_vx", dimid_calib_, calib);
}

auto radar_calibration_group::set_noise_vx(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "noise_vx", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::receiver_gain_hc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_gain_hc", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_gain_hc(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "receiver_gain_hc", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::receiver_gain_vc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_gain_vc", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_gain_vc(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "receiver_gain_vc", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::receiver_gain_hx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_gain_hx", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_gain_hx(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "receiver_gain_hx", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::receiver_gain_vx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_gain_vx", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_gain_vx(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "receiver_gain_vx", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::base_1km_hc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "base_1km_hc", dimid_calib_, calib);
}

auto radar_calibration_group::set_base_1km_hc(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "base_1km_hc", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBZ");
}

auto radar_calibration_group::base_1km_vc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "base_1km_vc", dimid_calib_, calib);
}

auto radar_calibration_group::set_base_1km_vc(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "base_1km_vc", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBZ");
}

auto radar_calibration_group::base_1km_hx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "base_1km_hx", dimid_calib_, calib);
}

auto radar_calibration_group::set_base_1km_hx(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "base_1km_hx", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBZ");
}

auto radar_calibration_group::base_1km_vx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "base_1km_vx", dimid_calib_, calib);
}

auto radar_calibration_group::set_base_1km_vx(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "base_1km_vx", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBZ");
}

auto radar_calibration_group::sun_power_hc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "sun_power_hc", dimid_calib_, calib);
}

auto radar_calibration_group::set_sun_power_hc(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "sun_power_hc", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::sun_power_vc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "sun_power_vc", dimid_calib_, calib);
}

auto radar_calibration_group::set_sun_power_vc(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "sun_power_vc", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::sun_power_hx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "sun_power_hx", dimid_calib_, calib);
}

auto radar_calibration_group::set_sun_power_hx(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "sun_power_hx", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::sun_power_vx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "sun_power_vx", dimid_calib_, calib);
}

auto radar_calibration_group::set_sun_power_vx(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "sun_power_vx", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::noise_source_power_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "noise_source_power_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_noise_source_power_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "noise_source_power_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::noise_source_power_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "noise_source_power_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_noise_source_power_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "noise_source_power_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::power_measure_loss_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "power_measure_loss_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_power_measure_loss_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "power_measure_loss_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::power_measure_loss_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "power_measure_loss_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_power_measure_loss_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "power_measure_loss_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::coupler_forward_loss_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "coupler_forward_loss_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_coupler_forward_loss_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "coupler_forward_loss_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::coupler_forward_loss_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "coupler_forward_loss_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_coupler_forward_loss_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "coupler_forward_loss_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::zdr_correction(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "zdr_correction", dimid_calib_, calib);
}

auto radar_calibration_group::set_zdr_correction(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "zdr_correction", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::ldr_correction_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "ldr_correction_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_ldr_correction_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "ldr_correction_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::ldr_correction_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "ldr_correction_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_ldr_correction_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "ldr_correction_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dB");
}

auto radar_calibration_group::system_phidp(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "system_phidp", dimid_calib_, calib);
}

auto radar_calibration_group::set_system_phidp(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "system_phidp", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto radar_calibration_group::test_power_h(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "test_power_h", dimid_calib_, calib);
}

auto radar_calibration_group::set_test_power_h(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "test_power_h", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::test_power_v(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "test_power_v", dimid_calib_, calib);
}

auto radar_calibration_group::set_test_power_v(size_t calib, float val) -> void
{
  if (auto [varid, isnew] = var_set1(ncid_, "test_power_v", dimid_calib_, calib, val); isnew)
    att_set(ncid_, varid, "units", "dBm");
}

auto radar_calibration_group::receiver_slope_hc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_slope_hc", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_slope_hc(size_t calib, float val) -> void
{
  var_set1(ncid_, "receiver_slope_hc", dimid_calib_, calib, val);
}

auto radar_calibration_group::receiver_slope_vc(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_slope_vc", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_slope_vc(size_t calib, float val) -> void
{
  var_set1(ncid_, "receiver_slope_vc", dimid_calib_, calib, val);
}

auto radar_calibration_group::receiver_slope_hx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_slope_hx", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_slope_hx(size_t calib, float val) -> void
{
  var_set1(ncid_, "receiver_slope_hx", dimid_calib_, calib, val);
}

auto radar_calibration_group::receiver_slope_vx(size_t calib) const -> std::optional<float>
{
  return var_get1<float>(ncid_, "receiver_slope_vx", dimid_calib_, calib);
}

auto radar_calibration_group::set_receiver_slope_vx(size_t calib, float val) -> void
{
  var_set1(ncid_, "receiver_slope_vx", dimid_calib_, calib, val);
}

auto georeference_correction_group::azimuth_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "azimuth_correction");
}

auto georeference_correction_group::set_azimuth_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "azimuth_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::elevation_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "elevation_correction");
}

auto georeference_correction_group::set_elevation_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "elevation_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::range_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "range_correction");
}

auto georeference_correction_group::set_range_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "range_correction", val); isnew)
    att_set(ncid_, varid, "units", "meters");
}

auto georeference_correction_group::longitude_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "longitude_correction");
}

auto georeference_correction_group::set_longitude_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "longitude_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::latitude_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "latitude_correction");
}

auto georeference_correction_group::set_latitude_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "latitude_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::pressure_altitude_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "pressure_altitude_correction");
}

auto georeference_correction_group::set_pressure_altitude_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "pressure_altitude_correction", val); isnew)
    att_set(ncid_, varid, "units", "meters");
}

auto georeference_correction_group::radar_altitude_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "radar_altitude_correction");
}

auto georeference_correction_group::set_radar_altitude_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "radar_altitude_correction", val); isnew)
    att_set(ncid_, varid, "units", "meters");
}

auto georeference_correction_group::eastward_ground_speed_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "eastward_ground_speed_correction");
}

auto georeference_correction_group::set_eastward_ground_speed_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "eastward_ground_speed_correction", val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_correction_group::northward_ground_speed_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "northward_ground_speed_correction");
}

auto georeference_correction_group::set_northward_ground_speed_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "northward_ground_speed_correction", val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_correction_group::vertical_velocity_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "vertical_velocity_correction");
}

auto georeference_correction_group::set_vertical_velocity_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "vertical_velocity_correction", val); isnew)
    att_set(ncid_, varid, "units", "m/s");
}

auto georeference_correction_group::heading_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "heading_correction");
}

auto georeference_correction_group::set_heading_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "heading_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::roll_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "roll_correction");
}

auto georeference_correction_group::set_roll_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "roll_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::pitch_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "pitch_correction");
}

auto georeference_correction_group::set_pitch_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "pitch_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::drift_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "drift_correction");
}

auto georeference_correction_group::set_drift_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "drift_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::rotation_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "rotation_correction");
}

auto georeference_correction_group::set_rotation_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "rotation_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

auto georeference_correction_group::tilt_correction() const -> std::optional<float>
{
  return var_get<float>(ncid_, "tilt_correction");
}

auto georeference_correction_group::set_tilt_correction(float val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "tilt_correction", val); isnew)
    att_set(ncid_, varid, "units", "degrees");
}

static auto open_or_create(char const* path, io_mode mode)
{
  int ncid;
  if (mode == io_mode::create)
  {
    if (auto status = nc_create(path, NC_NETCDF4, &ncid))
      throw error{"Failed to create file", status, -1, NC_GLOBAL, path};
  }
  else
  {
    if (auto status = nc_open(path, mode == io_mode::read_only ? NC_NOWRITE : NC_WRITE, &ncid))
      throw error{"Failed to open file", status, -1, NC_GLOBAL, path};
  }
  return ncid;
}

volume::volume(std::filesystem::path const& path, io_mode mode)
  : group{open_or_create(path.c_str(), mode)}
  , dimid_sweep_{-1}
  , sweeps_{0}
{
  try
  {
    // for writeable files increase speed by not filling new variables with the _FillValue
    if (mode != io_mode::read_only)
    {
      int prior;
      if (auto status = nc_set_fill(ncid_, NC_NOFILL, &prior))
        throw error{"Failed to set fill mode", status, ncid_};
    }

    if (mode == io_mode::create)
    {
      // set our conventions and file type metadata
      att_set(ncid_, NC_GLOBAL, "Conventions", "CF-1.8, WMO CF-1.0, ACDD-1.3");
      att_set(ncid_, NC_GLOBAL, "version", "2.0");

      // initialize the sweep dimension
      dimid_sweep_ = wrap_nc_def_dim(ncid_, "sweep", NC_UNLIMITED);

      // initialize the sweep group names variable
      int varid;
      if (auto status = nc_def_var(ncid_, "sweep_group_name", NC_STRING, 1, &dimid_sweep_, &varid))
        throw error{"Failed to create variable", status, ncid_, NC_GLOBAL, "sweep_group_name"};
    }
    else
    {
      // do we have a 'sweep' dimension?
      if (dim_exists(ncid_, "sweep"))
      {
        dimid_sweep_ = wrap_nc_inq_dimid(ncid_, "sweep");
        sweeps_ = wrap_nc_inq_dimlen(ncid_, dimid_sweep_);
      }
      else
      {
        // fallback to counting based on our WMO mandated sweep group names
        while (group_exists(ncid_, ("sweep_" + std::to_string(sweeps_)).c_str()))
          ++sweeps_;
      }
    }
  }
  catch (...)
  {
    nc_close(ncid_);
    throw;
  }
}

volume::volume(volume&& rhs) noexcept
  : group{rhs.ncid_}
  , dimid_sweep_{rhs.dimid_sweep_}
  , sweeps_{rhs.sweeps_}
{
  rhs.ncid_ = 0;
}

auto volume::operator=(volume&& rhs) noexcept -> volume&
{
  nc_close(ncid_);
  ncid_ = rhs.ncid_;
  dimid_sweep_ = rhs.dimid_sweep_;
  sweeps_ = rhs.sweeps_;
  rhs.ncid_ = 0;
  return *this;
}

volume::~volume()
{
  nc_close(ncid_);
}

auto volume::flush() -> void
{
  if (auto status = nc_sync(ncid_))
    throw error{"Failed to flush file to disk", status, ncid_};
}

auto volume::sweep(size_t index) const -> fm301::sweep
{
  // if we have a sweep_group_name variable use it directly, otherwise fallback to WMO mandated sweep group names
  std::string name;
  if (dimid_sweep_ != -1)
  {
    if (auto val = var_get1<std::string>(ncid_, "sweep_group_name", dimid_sweep_, index))
      name = std::move(*val);
  }
  if (name.empty())
    name = "sweep_" + std::to_string(index);

  int grpid;
  if (auto status = nc_inq_grp_ncid(ncid_, name.c_str(), &grpid))
    throw error{"Failed to find sweep group", status, ncid_, NC_GLOBAL, name.c_str()};
  return fm301::sweep{grpid};
}

auto volume::sweep(std::string const& name) -> fm301::sweep
{
  int grpid;
  if (auto status = nc_inq_grp_ncid(ncid_, name.c_str(), &grpid))
    throw error{"Failed to find sweep group", status, ncid_, NC_GLOBAL, name.c_str()};
  return fm301::sweep{grpid};
}

auto volume::sweep_add(size_t rays, size_t bins, size_t frequencies, size_t prts, std::string name) -> fm301::sweep
{
  // use WMO mandated sweep group names by default
  if (name.empty())
    name = "sweep_" + std::to_string(sweeps_);

  // add our new sweep name to the global index variable
  if (dimid_sweep_ != -1)
  {
    int varid;
    if (auto status = nc_inq_varid(ncid_, "sweep_group_name", &varid))
      throw error{"Failed to find variable", status, ncid_, NC_GLOBAL, "sweep_group_name"};
    size_t index = sweep_count();
    char const* data = name.c_str();
    if (auto status = nc_put_var1_string(ncid_, varid, &index, &data))
      throw error{"Failed to write variable", status, ncid_, NC_GLOBAL, "sweep_group_name"};
  }

  auto grpid = wrap_nc_def_grp(ncid_, name.c_str());
  ++sweeps_;
  return fm301::sweep{grpid, rays, bins, frequencies, prts};
}

auto volume::radar_parameters() const -> std::optional<radar_parameters_group>
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "radar_parameters", &grpid))
    return std::nullopt;
  return radar_parameters_group{grpid};
}

auto volume::radar_parameters_add() -> radar_parameters_group
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "radar_parameters", &grpid))
    return radar_parameters_group{wrap_nc_def_grp(ncid_, "radar_parameters")};
  return radar_parameters_group{grpid};
}

auto volume::radar_calibration() const -> std::optional<radar_calibration_group>
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "radar_calibration", &grpid))
    return std::nullopt;
  return radar_calibration_group{grpid};
}

auto volume::radar_calibration_add(size_t calibration_count) -> radar_calibration_group
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "radar_calibration", &grpid))
    return radar_calibration_group{wrap_nc_def_grp(ncid_, "radar_calibration"), calibration_count};

  auto ret = radar_calibration_group{grpid};
  if (ret.calibration_count() != calibration_count)
    throw error{"Failed to add radar_calibration group with different calib count"};
  return ret;
}

auto volume::lidar_parameters() const -> std::optional<lidar_parameters_group>
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "lidar_parameters", &grpid))
    return std::nullopt;
  return lidar_parameters_group{grpid};
}

auto volume::lidar_parameters_add() -> lidar_parameters_group
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "lidar_parameters", &grpid))
    return lidar_parameters_group{wrap_nc_def_grp(ncid_, "lidar_parameters")};
  return lidar_parameters_group{grpid};
}

auto volume::lidar_calibration() const -> std::optional<lidar_calibration_group>
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "lidar_calibration", &grpid))
    return std::nullopt;
  return lidar_calibration_group{grpid};
}

auto volume::lidar_calibration_add() -> lidar_calibration_group
{
  int grpid;
  if (nc_inq_grp_ncid(ncid_, "lidar_calibration", &grpid))
    return lidar_calibration_group{wrap_nc_def_grp(ncid_, "lidar_calibration")};
  return lidar_calibration_group{grpid};
}

auto volume::conventions() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "Conventions");
}

auto volume::set_conventions(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "Conventions", val);
}

auto volume::version() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "version");
}

auto volume::set_version(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "version", val);
}

auto volume::title() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "title");
}

auto volume::set_title(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "title", val);
}

auto volume::institution() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "institution");
}

auto volume::set_institution(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "institution", val);
}

auto volume::references() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "references");
}

auto volume::set_references(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "references", val);
}

auto volume::source() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "source");
}

auto volume::set_source(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "source", val);
}

auto volume::history() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "history");
}

auto volume::set_history(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "history", val);
}

auto volume::comment() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "comment");
}

auto volume::set_comment(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "comment", val);
}

auto volume::instrument_name() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "instrument_name");
}

auto volume::set_instrument_name(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "instrument_name", val);
}

auto volume::site_name() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "site_name");
}

auto volume::set_site_name(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "site_name", val);
}

auto volume::scan_name() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "scan_name");
}

auto volume::set_scan_name(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "scan_name", val);
}

auto volume::scan_id() const -> std::optional<int>
{
  return att_get<int>(ncid_, NC_GLOBAL, "scan_id");
}

auto volume::set_scan_id(int val) -> void
{
  att_set(ncid_, NC_GLOBAL, "scan_id", val);
}

auto volume::platform_is_mobile() const -> std::optional<bool>
{
  return parse_bool(att_get<std::string>(ncid_, NC_GLOBAL, "platform_is_mobile"));
}

auto volume::set_platform_is_mobile(bool val) -> void
{
  att_set(ncid_, NC_GLOBAL, "platform_is_mobile", format_bool(val));
}

auto volume::ray_times_increase() const -> std::optional<bool>
{
  return parse_bool(att_get<std::string>(ncid_, NC_GLOBAL, "ray_times_increase"));
}

auto volume::set_ray_times_increase(bool val) -> void
{
  att_set(ncid_, NC_GLOBAL, "ray_times_increase", format_bool(val));
}

auto volume::simulated_data() const -> std::optional<bool>
{
  return parse_bool(att_get<std::string>(ncid_, NC_GLOBAL, "simulated_data"));
}

auto volume::set_simulated_data(bool val) -> void
{
  att_set(ncid_, NC_GLOBAL, "simulated_data", format_bool(val));
}

auto volume::wmo_cf_profile() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "wmo__cf_profile");
}

auto volume::set_wmo_cf_profile(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "wmo__cf_profile", val);
}

auto volume::wmo_data_category() const -> std::optional<int>
{
  return att_get<int>(ncid_, NC_GLOBAL, "wmo__data_category");
}

auto volume::set_wmo_data_category(int val) -> void
{
  att_set(ncid_, NC_GLOBAL, "wmo__data_category", val);
}

auto volume::wmo_originating_centre() const -> std::optional<int>
{
  return att_get<int>(ncid_, NC_GLOBAL, "wmo__originating_centre");
}

auto volume::set_wmo_originating_centre(int val) -> void
{
  att_set(ncid_, NC_GLOBAL, "wmo__originating_centre", val);
}

auto volume::wmo_originating_sub_centre() const -> std::optional<int>
{
  return att_get<int>(ncid_, NC_GLOBAL, "wmo__originating_sub_centre");
}

auto volume::set_wmo_originating_sub_centre(int val) -> void
{
  att_set(ncid_, NC_GLOBAL, "wmo__originating_sub_centre", val);
}

auto volume::wmo_update_sequence_number() const -> std::optional<int>
{
  return att_get<int>(ncid_, NC_GLOBAL, "wmo__update_sequence_number");
}

auto volume::set_wmo_update_sequence_number(int val) -> void
{
  att_set(ncid_, NC_GLOBAL, "wmo__update_sequence_number", val);
}

auto volume::wmo_id() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "wmo__id");
}

auto volume::set_wmo_id(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "wmo__id", val);
}

auto volume::wmo_wsi() const -> std::optional<std::string>
{
  return att_get<std::string>(ncid_, NC_GLOBAL, "wmo__wsi");
}

auto volume::set_wmo_wsi(std::string const& val) -> void
{
  att_set(ncid_, NC_GLOBAL, "wmo__wsi", val);
}

auto volume::volume_number() const -> std::optional<int>
{
  return var_get<int>(ncid_, "volume_number");
}

auto volume::set_volume_number(int val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "volume_number", val); isnew)
    att_set(ncid_, varid, "long_name", "Volume number");
}

auto volume::platform_type() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "platform_type");
}

auto volume::set_platform_type(std::string const& val) -> void
{
  var_set(ncid_, "platform_type", val);
}

auto volume::instrument_type() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "instrument_type");
}

auto volume::set_instrument_type(std::string const& val) -> void
{
  var_set(ncid_, "instrument_type", val);
}

auto volume::primary_axis() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "primary_axis");
}

auto volume::set_primary_axis(std::string const& val) -> void
{
  var_set(ncid_, "primary_axis", val);
}

auto volume::time_coverage_start() const -> std::optional<std::chrono::system_clock::time_point>
{
  // try to read as a variable
  if (auto raw_val = var_get<double>(ncid_, "time_coverage_start"))
  {
    auto varid = wrap_nc_inq_varid(ncid_, "time_coverage_start");
    auto units = att_get<std::string>(ncid_, varid, "units");
    if (!units)
      throw error{"Failed to read attribute", NC_NOERR, ncid_, varid, "units"};
    auto since_time = parse_seconds_since_time(*units);
    auto val = std::chrono::duration<double>(*raw_val);
    return since_time + std::chrono::duration_cast<std::chrono::system_clock::duration>(val);
  }

  // try to read as an attribute
  if (auto raw_val = att_get<std::string>(ncid_, NC_GLOBAL, "time_coverage_start"))
    return parse_iso8601_time(*raw_val);

  return std::nullopt;
}

auto volume::set_time_coverage_start(std::chrono::system_clock::time_point val) -> void
{
  // for convenience we always store this as 0.0 in the varaible, and set the "since" time to val
  // this means we always update the units since that's where the real information is stored
  auto [varid, isnew] = var_set(ncid_, "time_coverage_start", 0.0);
  att_set(ncid_, varid, "units", format_seconds_since_time(val));
  if (isnew)
  {
    att_set(ncid_, varid, "standard_name", "time");
    att_set(ncid_, varid, "long_name", "Time of first ray in file");
    att_set(ncid_, varid, "calendar", "standard");
  }

  // duplicated global attribute
  att_set(ncid_, NC_GLOBAL, "time_coverage_start", format_iso8601_time(val));
}

auto volume::time_coverage_end() const -> std::optional<std::chrono::system_clock::time_point>
{
  // try to read as a variable
  if (auto raw_val = var_get<double>(ncid_, "time_coverage_end"))
  {
    auto varid = wrap_nc_inq_varid(ncid_, "time_coverage_end");
    auto units = att_get<std::string>(ncid_, varid, "units");
    if (!units)
      throw error{"Failed to read attribute", NC_NOERR, ncid_, varid, "units"};
    auto since_time = parse_seconds_since_time(*units);
    auto val = std::chrono::duration<double>(*raw_val);
    return since_time + std::chrono::duration_cast<std::chrono::system_clock::duration>(val);
  }

  // try to read as an attribute
  if (auto raw_val = att_get<std::string>(ncid_, NC_GLOBAL, "time_coverage_end"))
    return parse_iso8601_time(*raw_val);

  return std::nullopt;
}

auto volume::set_time_coverage_end(std::chrono::system_clock::time_point val) -> void
{
  // for convenience we always store this as 0.0 in the varaible, and set the "since" time to val
  // this means we always update the units since that's where the real information is stored
  auto [varid, isnew] = var_set(ncid_, "time_coverage_end", 0.0);
  att_set(ncid_, varid, "units", format_seconds_since_time(val));
  if (isnew)
  {
    att_set(ncid_, varid, "long_name", "Time of last ray in file");
    att_set(ncid_, varid, "calendar", "standard");
  }

  // duplicated global attribute
  att_set(ncid_, NC_GLOBAL, "time_coverage_end", format_iso8601_time(val));
}

auto volume::latitude() const -> std::optional<double>
{
  return var_get<double>(ncid_, "latitude");
}

auto volume::set_latitude(double val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "latitude", val); isnew)
  {
    att_set(ncid_, varid, "long_name", "Latitude of instrument");
    att_set(ncid_, varid, "units", "degrees_north");
  }
}

auto volume::longitude() const -> std::optional<double>
{
  return var_get<double>(ncid_, "longitude");
}

auto volume::set_longitude(double val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "longitude", val); isnew)
  {
    att_set(ncid_, varid, "long_name", "Longitude of instrument");
    att_set(ncid_, varid, "units", "degrees_east");
  }
}

auto volume::altitude() const -> std::optional<double>
{
  return var_get<double>(ncid_, "altitude");
}

auto volume::set_altitude(double val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "altitude", val); isnew)
  {
    att_set(ncid_, varid, "long_name", "Altitude of instrument above mean sea level");
    att_set(ncid_, varid, "units", "meters");
  }
}

auto volume::altitude_agl() const -> std::optional<double>
{
  return var_get<double>(ncid_, "altitude_agl");
}

auto volume::set_altitude_agl(double val) -> void
{
  if (auto [varid, isnew] = var_set(ncid_, "altitude_agl", val); isnew)
  {
    att_set(ncid_, varid, "long_name", "Altitude of instrument above ground level");
    att_set(ncid_, varid, "units", "meters");
  }
}

auto volume::status_str() const -> std::optional<std::string>
{
  return var_get<std::string>(ncid_, "status_str");
}

auto volume::set_status_str(std::string const& val) -> void
{
  var_set(ncid_, "status_str", val);
}
