#----------------------------------------------------------------------------------------------------------------------
# FM301 C++ API and Toolkit
#
# Copyright 2021 Commonwealth of Australia, Bureau of Meteorology
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.
#----------------------------------------------------------------------------------------------------------------------

# try to get a hint from pkg-config
find_package(PkgConfig)
if (PKG_CONFIG_FOUND)
  pkg_check_modules(PKG_netCDF QUIET netcdf)
endif()

# locate the library and headers
find_library(netCDF_LIBRARY NAMES "netcdf" PATHS ${PKG_netCDF_LIBRARY_DIRS})
find_path(netCDF_INCLUDE_DIR NAMES "netcdf.h" PATHS ${PKG_netCDF_INCLUDE_DIRS})
set(netCDF_VERSION ${PKG_netCDF_VERSION})

# standard processing
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  netCDF
  REQUIRED_VARS
  netCDF_LIBRARY
  netCDF_INCLUDE_DIR
  VERSION_VAR netCDF_VERSION
)

if (netCDF_FOUND)
  set(netCDF_LIBRARIES ${netCDF_LIBRARY})
  set(netCDF_INCLUDE_DIRS ${netCDF_INCLUDE_DIR})
  set(netCDF_DEFINITIONS ${PKG_netCDF_CFLAGS_OTHER})
endif()

# make an imported library
if (netCDF_FOUND AND NOT TARGET netCDF::netCDF)
  add_library(netCDF::netCDF UNKNOWN IMPORTED)
  set_target_properties(netCDF::netCDF PROPERTIES
    IMPORTED_LOCATION "${netCDF_LIBRARY}"
    INTERFACE_COMPILE_OPTIONS "${PKG_netCDF_CFLAGS_OHTER}"
    INTERFACE_INCLUDE_DIRECTORIES "${netCDF_INCLUDE_DIR}"
  )
endif()

mark_as_advanced(
  netCDF_INCLUDE_DIR
  netCDF_LIBRARY
)
